sizes = [int(z) for z in input().split()]
n = sizes[0]
m = sizes[1]
n_seq = [int(x) for x in input().split()]
m_seq = [int(x) for x in input().split()]

def solve():
    new = []
    s = sorted(m_seq)
    for x in n_seq:
        for y in s:
            if x>y:
                pass
            else:
                new.append(y)
                s.remove(y)

    return new

print(solve())
