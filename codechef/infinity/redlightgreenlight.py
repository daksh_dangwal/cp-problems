t = int(input())
tests = []
ans = []

def solve(k, n):
    p = 0 # answer
    for x in n:
        if x>k:
            p+=1
    return p

for _ in range(t):
    arr = [int(x) for x in input().split()]
    n = arr[0]
    k = arr[1]
    n = [int(x) for x in input().split()]

    ans.append(solve(k, n))

for x in ans: print(x)