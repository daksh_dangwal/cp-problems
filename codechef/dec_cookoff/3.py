t = int(input())
tests = [[int(input()), input()] for _ in range(t)]
ans = []

def solve(s):
    zero = s.count("0")
    one = s.count("1")

    if zero>one:
        if one == 0: return 0
        return one-1
    elif one>zero:
        if zero == 0: return 0
        return zero-1
    else:
        return zero-1

for test in tests: ans.append(solve(test[1]))
for a in ans: print(a)