t = int(input())
tests = []
ans = []
for _ in range(t):
    tests.append([int(input()), input()])

def solve(seq):
    vals = []
    seq = [int(x) for x in seq]
    for i in range(seq):
        for j in range(seq):
            if i==j: break
            for k in range(seq):
                if j==k: break
                vals.append((a[i] - a[j])*a[k])
    vals.sort()
    return vals[-1]


for test in tests:
    ans.append(test[1])
for a in ans: print(a)